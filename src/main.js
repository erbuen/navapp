import React, { createContext, Component } from 'react';
import { createAppContainer, createStackNavigator } from 'react-navigation';
import HomeScreen from './screens/Home';
import DetailsScreen from './screens/Details';

export const { Provider, Consumer } = createContext({});

const AppNavigator = createStackNavigator({
    Home: {
        screen: HomeScreen
    },
    Details: {
        screen: DetailsScreen
    }
});
  
const AppContainer = createAppContainer(AppNavigator);

export default class extends Component {

    state = { 
        store: { 
            myText: "Click the button"
        },
        actions: { 
            sayHello: () => this.setState({store:{myText:"Hello World!"}})
        }
    }

    render() { 
        return(
            <Provider value={this.state}>
                <AppContainer />
            </Provider>
        );
    }

}