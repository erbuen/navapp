import React from 'react';
import { Text, View, StyleSheet, Button} from 'react-native';
import { Consumer } from '../../main';

export default class HomeScreen extends React.Component {

    static navigationOptions = { 
      title: 'Home'
    };
  
    render() {
      return (
        <View style={styles.main}>

            <Consumer>
                { 
                    (value) => <Text>{value.store.myText}</Text>
                }    
            </Consumer>

            <Consumer>
                {
                    (value) => <Button onPress={()=>value.actions.sayHello()} title="Say hello" />
                }        
            </Consumer> 

          <Button onPress={()=>this.props.navigation.navigate('Details')} title='GO GO GO'/>

        </View>
      );
    }
  }

  const styles = StyleSheet.create({
    main: { 
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
    }
  });