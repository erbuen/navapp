import React from 'react';
import { Text, View, StyleSheet, Button} from 'react-native';

export default class DetailsScreen extends React.Component {
  
    static navigationOptions = { 
      title: 'Details'
    };
  
    render() {
      return (
        <View style={styles.main}>
          <Text>Details Screen</Text>
          <Button onPress={()=>this.props.navigation.navigate('Home')} title='BACK STREET IS BACK'/>
        </View>
      );
    }
  }

  const styles = StyleSheet.create({
    main: { 
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
    }
  });